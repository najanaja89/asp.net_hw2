﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace asp.net_hw2.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Iin { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        //public virtual VisitTable VisitTable { get; set; }
    }
}