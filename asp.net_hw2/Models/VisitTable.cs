﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace asp.net_hw2.Models
{
    public class VisitTable
    {
        public int Id { get; set; }

        //[ForeignKey("Person")]
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Specialty { get; set; }
        public string Diagnosis { get; set; }
        public string Complaints { get; set; }
        public DateTime VisitDate { get; set; }

        //public virtual Person Person { get; set; }
    }
}