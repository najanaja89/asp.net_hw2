﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using asp.net_hw2.Models;

namespace asp.net_hw2.Controllers
{
    public class VisitTablesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: VisitTables
        public ActionResult Index(int? id)
        {
            ViewBag.Id = id;
            if (id == null) RedirectToAction("Index", "Person");
            return View(db.VisitsTable.Where(x=>x.PersonId==id).ToList());
        }

        // GET: VisitTables/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitTable visitTable = db.VisitsTable.Find(id);
            if (visitTable == null)
            {
                return HttpNotFound();
            }
            return View(visitTable);
        }

        // GET: VisitTables/Create
        public ActionResult Create(int id)
        {
          
            var visitTable = new VisitTable();
            ViewBag.Id = visitTable.PersonId;
            visitTable.PersonId = id;
            return View(visitTable);
        }

        // POST: VisitTables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,Specialty,Diagnosis,Complaints,VisitDate")] VisitTable visitTable)
        {
            if (ModelState.IsValid)
            {
                db.VisitsTable.Add(visitTable);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = visitTable.PersonId });
            }

            return View(visitTable);
        }

        // GET: VisitTables/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitTable visitTable = db.VisitsTable.Find(id);
            if (visitTable == null)
            {
                return HttpNotFound();
            }
            return View(visitTable);
        }

        // POST: VisitTables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name,Surname,Specialty,Diagnosis,Complaints,VisitDate")] VisitTable visitTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(visitTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(visitTable);
        }

        // GET: VisitTables/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitTable visitTable = db.VisitsTable.Find(id);
            if (visitTable == null)
            {
                return HttpNotFound();
            }
            return View(visitTable);
        }

        // POST: VisitTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VisitTable visitTable = db.VisitsTable.Find(id);
            db.VisitsTable.Remove(visitTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
